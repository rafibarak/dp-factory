package singelton;

public class SingleObject {

	public static SingleObject instance = new SingleObject();
	
	public SingleObject() {
	
	}
	
	public static SingleObject getInstance() {
		return instance;
	}
	
	public void showMessage()
	{
		System.out.println("This is Singelton!");
	}
	
}
