package factory;

public class CarFactory {

	private String carType;

	public Car createCar(String carType)
	{
		if("HONDA".equals(carType))
		{
			return new CarHonda();
		}
		else if("GOLF".equals(carType))
		{
			return new CarGolf();
		}
		else if("VOLVO".equals(carType))
		{
			return new CarVolvo();
		}
		
		return null;
	}
	
	
	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}
	
	
	
}
