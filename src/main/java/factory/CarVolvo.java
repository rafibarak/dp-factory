package factory;

public class CarVolvo extends Car{

	private String carType ="VOLVO";
		
	@Override
	public String runCar() {
		return "I am running in " + carType + " Car";
	}
}
