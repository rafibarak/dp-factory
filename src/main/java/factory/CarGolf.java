package factory;

public class CarGolf extends Car {


	private String carType ="GOLF";
		
	@Override
	public String runCar() {
		return "I am running in  " + carType + " Car";
	}
}
