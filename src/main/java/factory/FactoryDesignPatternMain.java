package factory;

public class FactoryDesignPatternMain {

	public static void main(String[] args) {
		System.err.println("Start creating Cars...");
		CarFactory cf = new CarFactory();
		Car golf = cf.createCar("GOLF");
		System.out.println(golf.runCar());
		Car volvo = cf.createCar("VOLVO");
		System.out.println(volvo.runCar());
		Car honda = cf.createCar("HONDA");
		System.out.println(honda.runCar());
		System.err.println("End creating Cars...");

	}

}
