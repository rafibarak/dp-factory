package factory;

public class CarHonda extends Car {

	private String carType ="HONDA";
		
	@Override
	public String runCar() {
		return "I am running in " + carType + " Car";
	}
	
}
